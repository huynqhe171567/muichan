/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import model.User;
import model.DBConnect;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import model.Account;


/**
 *
 * @author acer
 */
public class UserDAO {
     public static Account GetAccountInformation(int id) {
        Account account = null;
        try {
            DBConnect db = new DBConnect();
            Connection conn = db.getConnection();
            if (conn != null) {
                String sql = "Select * from Account where id=" + id;
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    account = new Account();
                    account.setId(rs.getInt(1));
                    account.setEmail(rs.getString(2));
                    account.setPassword(rs.getString(3));
                    account.setRole(rs.getInt(4));
                    account.setStatus(rs.getInt(5));     
                }
                rs.close();
                st.close();
                conn.close();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return account;
    }
    public static void editUserProfile(User user) {
        try {
            DBConnect db = new DBConnect();
            Connection conn = db.getConnection();
            String sql = "UPDATE User "
                    + "SET firstName = ?,lastName = ? ,email = ?, phoneNumber = ?,address = ? ,image = ? "
                    + "WHERE id = " + user.getId();
            PreparedStatement st = conn.prepareStatement(sql);
            st = conn.prepareStatement(sql);
            st.setString(1, user.getFirstName());
            st.setString(2, user.getLastName());
            st.setString(3, user.getEmail());
            st.setString(4, user.getPhoneNumber());
            st.setString(5, user.getAddress());
            st.setString(6, user.getImage());
            // Execute the SQL statement
            st.executeUpdate();
            if (st.executeUpdate() != 1) {
                System.out.println("ERROR INSERTING User");
            }
            st.close();
            conn.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        }
   }


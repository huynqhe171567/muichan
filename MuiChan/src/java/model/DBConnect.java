package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBConnect {
    String instance = "";
    String serverName = "localhost";
    String portNumber = "1434";
    String dbName = "Muichan";
    String userID = "sa";
    String password = "123456";
    public Connection getConnection() throws ClassNotFoundException, SQLException{
        
        String url = "jdbc:sqlserver://" + serverName + ":" + portNumber + "\\" + instance + ";databaseName=" + dbName;
        if (instance == null || instance.trim().isEmpty()){
         url = "jdbc:sqlserver://" + serverName + ":" + portNumber +";databaseName=" + dbName;
         }
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
      return DriverManager.getConnection(url, userID, password);
      
    }
}


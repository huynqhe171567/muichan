
use MuiChan

go


CREATE TABLE Role (
    id int PRIMARY KEY IDENTITY(1,1),
    roleName CHAR(50) NOT NULL
);

-- Tạo bảng Account
CREATE TABLE Account (
    id INT PRIMARY KEY IDENTITY(1,1),
    email CHAR(50) NOT NULL,
    password CHAR(50) NOT NULL,
    role int NOT NULL,
    createDate DATE NOT NULL,
    status BIT NOT NULL,
    CONSTRAINT FK_Account_Role FOREIGN KEY (role) REFERENCES Role(id)
);

-- Tạo bảng User
CREATE TABLE [User] (
    id INT PRIMARY KEY IDENTITY(1,1),
    firstName CHAR(50) NOT NULL,
    lastName CHAR(50) NOT NULL,
     email CHAR(50) NOT NULL,
    phoneNumber CHAR(10) NOT NULL,
    address CHAR(100) NOT NULL,
    image NCHAR(100),
    accountId INT NOT NULL,
    CONSTRAINT FK_User_Account FOREIGN KEY (accountId) REFERENCES Account(id)
);

-- Tạo bảng Feedback
CREATE TABLE Feedback (
    ID INT PRIMARY KEY IDENTITY(1,1),
    note CHAR(50) NOT NULL,
    rank CHAR(50) NOT NULL,
    createDate DATE NOT NULL,
    userId INT NOT NULL,
    CONSTRAINT FK_Feedback_User FOREIGN KEY (userId) REFERENCES [User](id)
);

-- Tạo bảng Post
CREATE TABLE Post (
    id INT PRIMARY KEY IDENTITY(1,1),
    userId INT NOT NULL,
    year DATE NOT NULL,
    origin char (50) NOT NULL,
    image BIT NOT NULL,
    gearbox char (50) NOT NULL,
    engine char(50) NOT NULL,
    interiorColor char(50) NOT NULL,
    exteriorColor char(50) NOT NULL,
    numberOfSeats char(50) NOT NULL,
    numberOfDoors char(50) NOT NULL,
    postDate DATE NOT NULL,
    status BIT NOT NULL,
    descriptions nvarchar(1000),
    CONSTRAINT FK_Post_User FOREIGN KEY (userId) REFERENCES [User](id)
);

-- Tạo bảng Membership
CREATE TABLE Membership (
    id INT PRIMARY KEY IDENTITY(1,1),
    nameShowroom CHAR(50) NOT NULL,
    status BIT NOT NULL,
    startDate DATE NOT NULL,
    endDate DATE NOT NULL,
    membershipType INT NOT NULL,
    userId INT NOT NULL,
    CONSTRAINT FK_Membership_User FOREIGN KEY (userId) REFERENCES [User](id)
);

-- Tạo bảng SupportRequest
CREATE TABLE SupportRequest (
    id INT PRIMARY KEY IDENTITY(1,1),
    clientId INT NOT NULL,
    message NVARCHAR(MAX) NOT NULL,
    creatDate DATE NOT NULL,
    responseMessage NVARCHAR(MAX) NOT NULL,
    CONSTRAINT FK_SupportRequest_User FOREIGN KEY (clientId) REFERENCES [User](id)
);

-- Tạo bảng Brand
CREATE TABLE Brand (
    BrandID int  PRIMARY KEY IDENTITY(1,1),
    BrandName VARCHAR(50) NOT NULL
);

-- Tạo bảng Model
CREATE TABLE Model (
    ModelID int PRIMARY KEY  IDENTITY(1,1),
    ModelName VARCHAR(50)  NOT NULL
);

-- Tạo bảng Car
CREATE TABLE Car (
    id INT PRIMARY KEY IDENTITY(1,1),
    brandId int NOT NULL,
    modelId int NOT NULL,
    userId INT NOT NULL,
    CONSTRAINT FK_Car_Brand FOREIGN KEY (brandId) REFERENCES Brand(BrandID),
    CONSTRAINT FK_Car_Model FOREIGN KEY (modelId) REFERENCES Model(ModelID),
    CONSTRAINT FK_Car_User FOREIGN KEY (userId) REFERENCES [User](id)
);

-- Tạo bảng Orders
CREATE TABLE Orders (
    ID INT PRIMARY KEY IDENTITY(1,1),
    deposits FLOAT NOT NULL,
    order_date DATE NOT NULL,
    carId INT NOT NULL,
    userId INT NOT NULL,
    CONSTRAINT FK_Orders_Car FOREIGN KEY (carId) REFERENCES Car(id),
    CONSTRAINT FK_Orders_User FOREIGN KEY (userId) REFERENCES [User](id)
);
